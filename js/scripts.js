$(document).ready(function() {
    var variable = false;
    var usuario = false;
    $("#recovery").hide(0);
    $("#register").hide(0);
    $("#option").hide(0);
    $("#clientes_list").hide(0);
    $("#vehiculo_form").hide(0);
    $("#register_work").hide(0);
    $("#ocultar1").click(function() {
        $("#login").hide(0);
        $("#recovery").show(0);
    });
    
    $("#mostrar").click(function() {
        $("#recovery").hide(0);
        $("#login").show(0);
    });
    $("#ocultar2").click(function() {
        $("#login").hide(0);
        $("#option").show(0);
    });
    $("#volver2").click(function(){
        $("#option").hide(0);
        $("#login").show(0);
    });
    $("#mostrar2").click(function() {
        $("#register").hide(0);
        $("#option").show(0);
    });
    $("#mostrar3").click(function() {
        $("#register_work").hide(0);
        $("#option").show(0);
    });
    $("#opcion1").click(function(){
        $("#option").hide(0);
        $("#register").show(0);
    });
    $("#opcion2").click(function(){
        $("#option").hide(0);
        $("#register_work").show(0);
    });
    $("#vehiculo").click(function(){
        $("#midatos_form").hide(0);
        $("#clientes_list").hide(0);
        $("#vehiculo_form").show(0);
    });
    $("#misdatos").click(function(){
        $("#vehiculo_form").hide(0);
        $("#clientes_list").hide(0);
        $("#midatos_form").show(0);
    });
    $("#clientes").click(function(){
        $("#vehiculo_form").hide(0);
        $("#midatos_form").hide(0);
        $("#clientes_list").show(0);
    });
    $("#facebook_link").click(function() {
        var opcion = confirm("Este enlace te llevará a facebook\n¿Desea continuar?");
        if (opcion == true) {
            window.location = "https://es-la.facebook.com/";
        }
    });
    $("#instagram_link").click(function() {
        var opcion = confirm("Este enlace te llevará a instagram\n¿Desea continuar?");
        if (opcion == true) {
            window.location = "https://www.instagram.com/";
        }
    });
    $("#twitter_link").click(function() {
        var opcion = confirm("Este enlace te llevará a twitter\n¿Desea continuar?");
        if (opcion == true) {
            window.location = "https://twitter.com/";
        }
    });
    $("#cliente").click(function() {
        $("#register_user").show(0);
        $("#register_worker").hide(0);
    });
    $("#trabajador").click(function() {
        $("#register_worker").show(0);
        $("#register_user").hide(0);
    });
    $("#menucito2").click(function() {
        var color = $("#elbody").css("color");
        if (variable == false) {
            $("#elbody").css({ 'background-color': 'rgb(255, 255, 255)' });
            $("#menucito2").html('<a href="javascript:void(0);" style="color: red;" ><i class="fa fa-moon-o"></i> Modo oscuro</a>');
            variable = true;
        } else {
            $("#elbody").css({ 'background-color': 'rgb(34, 34, 34)' });
            $("#menucito2").html('<a href="javascript:void(0);" style="color: red;" ><i class="fa fa-sun-o"></i> Modo claro</a>');
            variable = false;
        }
    });
    $("#buscador").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#buscado tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    
});

